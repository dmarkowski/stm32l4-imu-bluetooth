# STM32L4-IMU-BLUETOOTH
Embedded C project for STM32L4 that allows reading measurements from:
* external I2C IMU sensor,
* uC ADC voltage sensor, 

The measurements takes place in timer interrupt every 100 ms.
The data is stored in circle buffer and transfered via UART in main program loop.

## Table of content
* [Technologies](#technologies)
* [Hardware](#hardware)
    * [Components](#components)
    * [Connection](#connection)
* [Software design](#software-design)
    * [Software logic diagram](#software-logic-diagram)
    * [Microcontroller configuration](#microcontroller-configuration)
* [File structure](#file-structure)
* [Setup](#setup)

## Technologies
Following technologies were used in this project:
* C language - for implementing logic in microcontroller,
* HAL with CubeMX - for configuration of microcontroller's periferials and its documentation.
* Python - for script used to connect with microcontroller via Bluetooth and plot and save data in csv file.
## Hardware
### Components:
* [Board NUCLEO-L476RG](https://www.st.com/content/ccc/resource/technical/document/user_manual/98/2e/fa/4b/e0/82/43/b7/DM00105823.pdf/files/DM00105823.pdf/jcr:content/translations/en.DM00105823.pdf)
* [IMU sensor LSM9DS1](https://www.st.com/resource/en/datasheet/lsm9ds1.pdf)
* [Bluetooth HC-05](https://www.gme.cz/data/attachments/dsh.772-148.1.pdf)

### Connection:
* Bluetooth HC-05:
    * RX -> PB10 (NUCLEO CN10 25)
    * TX -> PB11 (NUCLEO CN10 14)
* IMU sensor LSM9DS1:
    * SCL -> PC0 (NUCLEO CN7 38)
    * SDA -> PC1 (NUCLEO CN7 36)

## Software design
### Software logic diagram
![Software logic diagram](resources/stm32-imu-bluetooth-logic-diagram.png)
### Microcontroller configuration
Refer to [documentation generated via CubeMX](resources/stm32l4-imu-bluetooth.pdf) 

## File structure
File structure in repository : 
* embedded - folder with microcontroller embedded code,
* bluetooth console - folder with Python script for connecting with the microcontroller via Bluetooth,
* resources - folder with images and files used in README doc.

## Setup
1. Connect hardware according to [Connection](#connection).
2. Build and deploy project to the microcontroller.
3. Pair Bluetooth module with computer.
4. Press RESET button on NUCLEO board.
5. Open console and run python script.

    ``` python bluetooth-console/run.py ```

6. Release RESET button.
5. Measurements started.
7. Wait till LED turn off.
9. Data is ploted.
10. Press CTRL + C to close figures.

