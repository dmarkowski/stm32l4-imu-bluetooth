import struct
import csv
import serial


def read_serial(log_file_path):
    ser = serial.Serial('COM19', 9600, serial.EIGHTBITS, serial.PARITY_NONE, serial.STOPBITS_ONE)
    with open(log_file_path, 'w+') as log_file:
        writer = csv.writer(log_file, delimiter=',', lineterminator='\n')
        while True:
            if ser.readable:
                first_byte = ser.read()
                if first_byte == b'\xaa':
                    ser.read(3)
                    data = struct.unpack('9f', ser.read(36))
                    if ser.read() != b'\x55':
                        print('Invalid frame')
                        break
                    print(data)
                    writer.writerow(data)
                elif first_byte == b'\x0F':
                    return
