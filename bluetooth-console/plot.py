import pandas as pd
import matplotlib.pyplot as plt


def plot_data(log_file_path):
    df = pd.read_csv(log_file_path)
    df.columns = ['mag x', 'mag y', 'mag z', 'gyro x', 'gyro y', 'gyro z', 'imu temp', 'uC volt', 'time']
    df.plot(x='time', y=['mag x', 'mag y', 'mag z'], title='magnetic induction vector [nT]')
    df.plot(x='time', y=['gyro x', 'gyro y', 'gyro z'], title='angular speed [°/s]')
    df.plot(x='time', y='imu temp', title='temperature [°C]')
    df.plot(x='time', y='uC volt', title='voltage [V]')
    print(df)
    plt.show()
