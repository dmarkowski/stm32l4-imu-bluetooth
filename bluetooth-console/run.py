from datetime import datetime
import os
from console import read_serial
from plot import plot_data

log_dir = 'logs/'
log_file_path = log_dir + datetime.now().strftime("%Y_%m_%d__%H_%M_%S") + '.csv'
if not os.path.exists(log_dir):
    os.makedirs(log_dir)

read_serial(log_file_path)

plot_data(log_file_path)
