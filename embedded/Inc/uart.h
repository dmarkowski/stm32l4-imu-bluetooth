/*
 * bluetooth.h
 *
 *  Created on: 16.02.2020
 *      Author: Dominik Markowski
 */

#ifndef UART_H_
#define UART_H_

#include "buffer.h"
#include "stm32l4xx_hal.h"

HAL_StatusTypeDef UART_Send(buffer_data data);
HAL_StatusTypeDef UART_Init();
HAL_StatusTypeDef UART_Stop();

#endif /* UART_H_ */
