/*
 * i2c3.h
 *
 *  Created on: 16.02.2021
 *      Author: Dominik Markowski
 */

#ifndef I2C_H
#define I2C_H

#include "stm32l4xx_hal.h"

HAL_StatusTypeDef I2C3_WriteMemory(uint16_t reg, uint16_t device, uint8_t data);
HAL_StatusTypeDef I2C3_ReadMemory(uint16_t reg, uint16_t device, uint8_t *data);

#endif
