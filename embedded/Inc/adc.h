/*
 * adc.h
 *
 *  Created on: 16.02.2020
 *      Author: Dominik Markowski
 */
#ifndef ADC_H_
#define ADC_H_

#include "stm32l4xx_hal.h"

HAL_StatusTypeDef ADC_GetUcVolt(float *volt);

#endif /* ADC_H_ */
