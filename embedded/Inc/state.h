/*
 * state.h
 *
 *  Created on: 16.02.2021
 *      Author: Dominik Markowski
 */

#ifndef STATE_H_
#define STATE_H_

typedef enum {
	STATE_IDLE, STATE_IN_PROGRESS, STATE_ERROR
} state;

extern state currentState;

#endif /* STATE_H_ */
