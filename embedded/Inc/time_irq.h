/*
 * time_irq.h
 *
 *  Created on:    16.02.2020
 *  Author:        Dominik Markowski
 */

#ifndef TIME_IRQ_H_
#define TIME_IRQ_H_


#include "stm32l4xx_hal.h"

void TIME_IRQ_Init();
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);

#endif /* TIME_IRQ_H_ */
