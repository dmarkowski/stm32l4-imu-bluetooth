/*
 * imu.h
 *
 *  Created on: 16.02.2021
 *      Author: Dominik Markowski
 */

#ifndef IMU_H
#define IMU_H

#include "stm32l4xx_hal.h"

/**
 * @brief	Initialize IMU's registers
 */
void IMU_Init(void);

/**
 * @brief	Reads magnetic field
 * 			unit: uT
 */
HAL_StatusTypeDef IMU_GetMag(float *data);

/**
 * @brief	Reads angular speed
 * 			unit: d/s
 */
HAL_StatusTypeDef IMU_GetGyro(float *data);

/**
 * @brief	Reads temperature
 * 			unit: Celsius degrees
 */
HAL_StatusTypeDef IMU_GetTemp(float *data);

#endif
