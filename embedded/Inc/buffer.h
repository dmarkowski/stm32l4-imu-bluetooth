/*
 * buffer.h
 *
 *  Created on: 16.02.2021
 *      Author: Dominik Markowski
 */

#ifndef BUFFER_H_
#define BUFFER_H_

#include "stm32l4xx_hal.h"

typedef struct {
	float mag[3];
	float gyro[3];
	float tempImu;
	float voltUc;
	float time;
} buffer_data;

typedef struct {
	volatile buffer_data * const buffer;
	uint8_t head_idx;
	uint8_t tail_idx;
} circ_buffer;

typedef enum {
	BUFF_EMPTY, BUFF_OK, BUFF_FULL
} buffer_result;

buffer_result BUFF_Pop(buffer_data *data);
buffer_result BUFF_Append(buffer_data data);
buffer_result BUFF_GetState();

#endif /* BUFFER_H_ */
