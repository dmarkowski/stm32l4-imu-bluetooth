/*
 * buffer.c
 *
 *  Created on: 24.02.2021
 *      Author: Dominik Markowski
 */

#include "buffer.h"

#define TX_BUFFER_SIZE 20

volatile buffer_data tx_buffer_arr[TX_BUFFER_SIZE];

volatile circ_buffer tx_buffer = { tx_buffer_arr, 0, 0 };

static uint8_t BUFF_IsFull();
static uint8_t BUFF_IsEmpty();

buffer_result BUFF_Append(buffer_data data) {

	if (BUFF_IsFull())
		return BUFF_FULL;

	uint8_t head_idx_temp = tx_buffer.head_idx + 1;

	if (head_idx_temp == TX_BUFFER_SIZE)
		head_idx_temp = 0;

	tx_buffer.buffer[head_idx_temp] = data;
	tx_buffer.head_idx = head_idx_temp;

	return BUFF_OK;
}

buffer_result BUFF_Pop(buffer_data *data) {

	if (BUFF_IsEmpty())
		return BUFF_EMPTY;

	tx_buffer.tail_idx++;

	if (tx_buffer.tail_idx == TX_BUFFER_SIZE)
		tx_buffer.tail_idx = 0;

	*data = tx_buffer.buffer[tx_buffer.tail_idx];

	return BUFF_OK;
}

buffer_result BUFF_GetState() {

	if (BUFF_IsFull())
		return BUFF_FULL;
	else if (BUFF_IsEmpty())
		return BUFF_EMPTY;
	else
		return BUFF_OK;
}

static uint8_t BUFF_IsFull() {
	uint8_t head_idx_temp = tx_buffer.head_idx + 1;

	if (head_idx_temp == TX_BUFFER_SIZE)
		head_idx_temp = 0;

	return head_idx_temp == tx_buffer.tail_idx;
}

static uint8_t BUFF_IsEmpty() {
	return tx_buffer.head_idx == tx_buffer.tail_idx;
}
