/*
 * uart.c
 *
 *  Created on: 28.02.2021
 *      Author: Dominik Markowski
 */

#include <stdio.h>
#include "uart.h"

#define FRAME_PREAM 0xAA
#define FRAME_END 0x55
#define FRAME_INIT 0xF0
#define FRAME_STOP 0x0F
#define FRAME_SIZE 8
#define UART_TIMEOUT 1000

extern UART_HandleTypeDef huart3;

typedef struct {
	uint8_t pream;
	buffer_data body;
	uint8_t end;
} frame_t;

HAL_StatusTypeDef UART_Send(buffer_data data) {

	frame_t frame = {(uint8_t)FRAME_PREAM, data, (uint8_t)FRAME_END};
	uint8_t size = sizeof(frame_t);
	HAL_StatusTypeDef status = HAL_UART_Transmit(&huart3, (uint8_t*) &frame, size, UART_TIMEOUT);

	return status;
}

HAL_StatusTypeDef UART_Init() {

	uint8_t frame = (uint8_t)FRAME_INIT;
	uint8_t size = sizeof(uint8_t);
	HAL_StatusTypeDef status = HAL_UART_Transmit(&huart3, (uint8_t*) &frame, size, UART_TIMEOUT);

	return status;
}

HAL_StatusTypeDef UART_Stop() {

	uint8_t frame = (uint8_t)FRAME_STOP;
	uint8_t size = sizeof(uint8_t);
	HAL_StatusTypeDef status = HAL_UART_Transmit(&huart3, (uint8_t*) &frame, size, UART_TIMEOUT);

	return status;
}
