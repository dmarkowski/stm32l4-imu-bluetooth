/*
 * i2c3.h
 *
 *  Created on:    16.02.2020
 *  Author:        Dominik Markowski
 * 	Description:   File containing functions for I2C communication.
 */

#include "i2c3.h"

#define I2C3_TIMEOUT 20

extern I2C_HandleTypeDef hi2c3;

HAL_StatusTypeDef I2C3_WriteMemory(uint16_t reg, uint16_t device, uint8_t data) {

	return HAL_I2C_Mem_Write(&hi2c3, device << 1, reg, 1, &data, 1, I2C3_TIMEOUT);
}

HAL_StatusTypeDef I2C3_ReadMemory(uint16_t reg, uint16_t device, uint8_t *data) {

	return HAL_I2C_Mem_Read(&hi2c3, device << 1, reg, 1, data, 1, I2C3_TIMEOUT);
}
