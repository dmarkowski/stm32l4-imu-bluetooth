/*
 * adc.c
 *
 *  Created on: 16.02.2020
 *      Author: Dominik Markowski
 */
#include "adc.h"

#define ADC_V_REF_INT ((uint16_t*)((uint32_t)0x1FFF75AA))

float adc_uC_v_a = 1;
float adc_uC_v_o = 0;

extern ADC_HandleTypeDef hadc1;

static float ADC_V_REF_ApplyLinearRegration(float volt) {
	return adc_uC_v_a * (volt - adc_uC_v_o);
}

static float ADC_CalculateUcVolt(uint32_t vRefRawCalc) {
	float volt = (3.0 * *ADC_V_REF_INT) / vRefRawCalc;
	return ADC_V_REF_ApplyLinearRegration(volt);
}

static HAL_StatusTypeDef ADC_ReadVoltRaw(uint32_t *data){

	HAL_ADC_Start(&hadc1);
	HAL_StatusTypeDef status = HAL_ADC_PollForConversion(&hadc1, 1);

	if (status == HAL_OK)
		*data = HAL_ADC_GetValue(&hadc1);

	return status;
}

HAL_StatusTypeDef ADC_GetUcVolt(float *volt) {

	uint32_t vRefRaw;
	HAL_StatusTypeDef status = ADC_ReadVoltRaw(&vRefRaw);
	if (status == HAL_OK)
		*volt = ADC_CalculateUcVolt(vRefRaw);

	return status;
}
