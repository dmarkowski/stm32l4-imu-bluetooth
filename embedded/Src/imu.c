/*
 * imu.c
 *
 *  Created on:    16.02.2020
 *  Author:        Dominik Markowski
 * 	Description:   File containing functions for IMU's setup and actions.
 */

#include "imu.h"
#include "i2c3.h"

/* gyroscope */
#define GYRO_ACC_TEMP	0x6A
#define CTRL_REG1_G 	0x10
#define CTRL_REG2_G 	0x11
#define CTRL_REG3_G 	0x12
#define ORIENT_CFG_G 	0x13
#define STATUS_REG_G	0x17
#define OUT_X_G_LSB 	0x18
#define OUT_X_G_MSB 	0x19
#define OUT_Y_G_LSB 	0x1A
#define OUT_Y_G_MSB 	0x1B
#define OUT_Z_G_LSB 	0x1C
#define OUT_Z_G_MSB 	0x1D

/* magnetometer */
#define MAGNETOMETER 	0x1C
#define CTRL_REG1_M 	0x20
#define CTRL_REG2_M 	0x21
#define CTRL_REG3_M 	0x22
#define CTRL_REG4_M 	0x23
#define CTRL_REG5_M 	0x24
#define STATUS_REG_M	0x27
#define OUT_X_M_LSB 	0x28
#define OUT_X_M_MSB 	0x29
#define OUT_Y_M_LSB 	0x2A
#define OUT_Y_M_MSB 	0x2B
#define OUT_Z_M_LSB 	0x2C
#define OUT_Z_M_MSB 	0x2D

/* thermometer */
#define OUT_TEMP_LSB	0x15
#define OUT_TEMP_MSB	0x16

float imu_gyro_a[9] = { 1, 0, 0, 0, 1, 0, 0, 0, 1 };
float imu_gyro_o[3] = { 0, 0, 0 };

float imu_mag_a[9] = { 1, 0, 0, 0, 1, 0, 0, 0, 1 };
float imu_mag_o[3] = { 0, 0, 0 };

float imu_temp_a = 0.0600083203514165;
float imu_temp_o = -458.342300228;

static void IMU_Convert(float* v, float* x, float *vec_a, float *vec_o) {
	v[0] = vec_a[0] * (x[0] - vec_o[0]) + vec_a[1] * (x[1] - vec_o[1])
			+ vec_a[2] * (x[2] - vec_o[2]);
	v[1] = vec_a[3] * (x[0] - vec_o[0]) + vec_a[4] * (x[1] - vec_o[1])
			+ vec_a[5] * (x[2] - vec_o[2]);
	v[2] = vec_a[6] * (x[0] - vec_o[0]) + vec_a[7] * (x[1] - vec_o[1])
			+ vec_a[8] * (x[2] - vec_o[2]);
}

static float IMU_TempConvert(float x, float a, float o) {
	return a * (x - o);
}

void IMU_Init(void) {
	I2C3_WriteMemory(CTRL_REG1_G, GYRO_ACC_TEMP, 0x20); // 00100000
	I2C3_WriteMemory(CTRL_REG2_G, GYRO_ACC_TEMP, 0x00);
	I2C3_WriteMemory(CTRL_REG3_G, GYRO_ACC_TEMP, 0x00);
	I2C3_WriteMemory(ORIENT_CFG_G, GYRO_ACC_TEMP, 0x00); // todo mo�na poprawic orietnacje
	I2C3_WriteMemory(CTRL_REG1_M, MAGNETOMETER, 0xF0); //11110000
	I2C3_WriteMemory(CTRL_REG2_M, MAGNETOMETER, 0x00);
	I2C3_WriteMemory(CTRL_REG3_M, MAGNETOMETER, 0x00);
	I2C3_WriteMemory(CTRL_REG4_M, MAGNETOMETER, 0x0C); // 00001100
	I2C3_WriteMemory(CTRL_REG5_M, MAGNETOMETER, 0x00);
}

static HAL_StatusTypeDef IMU_ReadSensor(uint16_t reg_msb, uint16_t reg_lsb,
		uint16_t device, int16_t *data) {

	uint8_t data1;
	uint8_t data2;

	HAL_StatusTypeDef status = I2C3_ReadMemory(reg_msb, device, &data1);
	if (status != HAL_OK) {
		return status;
	}

	status = I2C3_ReadMemory(reg_lsb, device, &data2);
	if (status != HAL_OK) {
		return status;
	}

	*data = data1 << 8;
	*data = *data | data2;
	return status;
}

HAL_StatusTypeDef IMU_GetMag(float *data) {

	int16_t raw[3];

	HAL_StatusTypeDef status = IMU_ReadSensor(OUT_X_M_MSB, OUT_X_M_LSB,
	MAGNETOMETER, raw);
	if (status != HAL_OK)
		return status;
	status = IMU_ReadSensor(OUT_Y_M_MSB, OUT_Y_M_LSB, MAGNETOMETER, raw + 1);
	if (status != HAL_OK)
		return status;
	status = IMU_ReadSensor(OUT_Z_M_MSB, OUT_Z_M_LSB, MAGNETOMETER, raw + 2);
	if (status != HAL_OK)
		return status;

	float mag_raw[3] = { (float) raw[0], (float) raw[1], raw[2] };

	IMU_Convert(data, mag_raw, imu_mag_a, imu_mag_o);

	return status;
}

HAL_StatusTypeDef IMU_GetGyro(float *data) {

	int16_t raw[3];

	HAL_StatusTypeDef status = IMU_ReadSensor(OUT_X_G_MSB, OUT_X_G_LSB,
	GYRO_ACC_TEMP, raw);
	if (status != HAL_OK)
		return status;
	status = IMU_ReadSensor(OUT_Y_G_MSB, OUT_Y_G_LSB, GYRO_ACC_TEMP, raw + 1);
	if (status != HAL_OK)
		return status;
	status = IMU_ReadSensor(OUT_Z_G_MSB, OUT_Z_G_LSB, GYRO_ACC_TEMP, raw + 2);
	if (status != HAL_OK)
		return status;

	float gyro_raw[3] = { (float) raw[0], (float) raw[1], (float) (-raw[2]) };

	IMU_Convert(data, gyro_raw, imu_gyro_a, imu_gyro_o);

	return status;
}

HAL_StatusTypeDef IMU_GetTemp(float *data) {

	int16_t buff;
	uint8_t data1;
	uint8_t data2;

	HAL_StatusTypeDef status = I2C3_ReadMemory(OUT_TEMP_MSB, GYRO_ACC_TEMP,
			&data1);
	if (status != HAL_OK) {
		return status;
	}

	status = I2C3_ReadMemory(OUT_TEMP_LSB, GYRO_ACC_TEMP, &data2);
	if (status != HAL_OK) {
		return status;
	}

	buff = data1 << 12;
	buff = buff | (data2 << 4);
	buff = buff >> 4;
	*data = IMU_TempConvert((float) buff, imu_temp_a, imu_temp_o);
	return status;
}
