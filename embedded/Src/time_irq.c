/*
 * time_irq.c
 *
 *  Created on:    16.02.2020
 *  Author:        Dominik Markowski
 */

#include "time_irq.h"
#include "imu.h"
#include "adc.h"
#include "state.h"
#include "buffer.h"

#define TOTAL_TIME 60

extern TIM_HandleTypeDef htim17;

uint32_t timeStamp = 0;
float period = 0.1;

void TIME_IRQ_Init() {
	htim17.Instance->PSC = (uint32_t) (period * 1000) - 1;
	htim17.Instance->ARR = (SystemCoreClock) / (1000) - 1;
	HAL_TIM_Base_Start_IT(&htim17);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	if (htim->Instance == TIM17) {
		timeStamp++;
		float time = ((float) timeStamp) * period;

		if (currentState == STATE_IN_PROGRESS) {
			float mag[3] = { 0 };
			HAL_StatusTypeDef status = IMU_GetMag(mag);
			if (status != HAL_OK) {
				currentState = STATE_ERROR;
				return;
			}

			float gyro[3] = { 0 };
			status = IMU_GetGyro(gyro);
			if (status != HAL_OK) {
				currentState = STATE_ERROR;
				return;
			}

			float imuTemp = 0;
			status = IMU_GetTemp(&imuTemp);
			if (status != HAL_OK) {
				currentState = STATE_ERROR;
				return;
			}

			float uCVolt = 0;
			status = ADC_GetUcVolt(&uCVolt);
			if (status != HAL_OK) {
				currentState = STATE_ERROR;
				return;
			}

			buffer_data data = { { mag[0], mag[1], mag[2] }, { gyro[0], gyro[1],
					gyro[2] }, imuTemp, uCVolt, time };

			buffer_result buffResult = BUFF_Append(data);
			if (buffResult != BUFF_OK) {
				currentState = STATE_ERROR;
			}

			if (time >= TOTAL_TIME)
				currentState = STATE_IDLE;

		} else if (currentState == STATE_ERROR) {
			HAL_GPIO_TogglePin(LED_USER_2_GPIO_Port, LED_USER_2_Pin);
		} else {
			HAL_GPIO_WritePin(LED_USER_2_GPIO_Port, LED_USER_2_Pin, GPIO_PIN_RESET);
		}
	}
}
